var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var initSize = 0.06
var font
var dimension = 11
var array = create2DArray(dimension, dimension, 0, false)
var randX = []
var randY = []

function preload() {
  font = loadFont('ttf/RobotoMono-Medium.ttf')
}

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  for (var i = 0; i < dimension; i++) {
    randX.push(Math.floor(Math.random() * dimension))
    randY.push(Math.floor(Math.random() * dimension))
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)
  textFont(font)
  textAlign(CENTER, CENTER)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < array.length; i++) {
    for (var j = 0; j < array[i].length; j++) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(dimension * 0.5)) * boardSize * initSize - boardSize * initSize * 0.8 * 0.07, windowHeight * 0.5 + (j - Math.floor(dimension * 0.5)) * boardSize * initSize - boardSize * initSize * 0.15 * 0.8)
      fill(255)
      noStroke()
      textSize(boardSize * initSize * 0.8)
      text(abs(Math.floor((array[i][j] + sin(frameCount * 0.1 + i  * 0.0871 + j * 1.797)))) % 2, 0, 0)

      fill(128)
      textSize(boardSize * initSize * 0.3)
      text(array[i][j], boardSize * initSize * 0.35, boardSize * initSize * 0.28)
      pop()
    }
  }

  for (var i = 0; i < dimension; i++) {
    push()
    translate(windowWidth * 0.5 + (randX[i] - Math.floor(dimension * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (randY[i] - Math.floor(dimension * 0.5)) * boardSize * initSize)
    fill(255)
    noStroke()
    rect(0, 0, boardSize * initSize * 0.925, boardSize * initSize * 0.925)
    pop()

    push()
    translate(windowWidth * 0.5 + (randX[i] - Math.floor(dimension * 0.5)) * boardSize * initSize - boardSize * initSize * 0.8 * 0.07, windowHeight * 0.5 + (randY[i] - Math.floor(dimension * 0.5)) * boardSize * initSize - boardSize * initSize * 0.15 * 0.8)
    fill(32)
    noStroke()
    textSize(boardSize * initSize * 0.8)
    text(array[randX[i]][randY[i]], 0, 0)

    fill(64)
    textSize(boardSize * initSize * 0.3)
    text(array[randX[i]][randY[i]], boardSize * initSize * 0.35, boardSize * initSize * 0.28)
    pop()
  }

  if (frameCount % 12 === 0) {
    for (var i = 0; i < dimension; i++) {
      randX[i] = Math.floor(Math.random() * dimension)
      randY[i] = Math.floor(Math.random() * dimension)
      array[randX[i]][randY[i]] = Math.floor(Math.random() * 2)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = Math.floor(Math.random() * 2)
      }
    }
    array[i] = columns
  }
  return array
}
